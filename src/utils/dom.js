const TIMEOUT = 5000;

const clickWhenPossible = async (locator, timeout = TIMEOUT) =>
  await driver.wait(() => driver.findElement(locator).then(el => el.click().then(() => true, () => false), () => false), timeout);

const textWhenPossible = async (locator, text, timeout = TIMEOUT) =>
  await driver.wait(() => driver.findElement(locator).then(el => el.sendKeys(text).then(() => true, () => false), () => false), timeout);

export { clickWhenPossible, textWhenPossible };
