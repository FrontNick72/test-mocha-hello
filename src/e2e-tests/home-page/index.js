import MainTest from "./main";
import { page } from "../../utils";

describe("Hello app Test", function() {
  before(async () => {
    await page.home();
  });

  describe("Main script", MainTest);
});
