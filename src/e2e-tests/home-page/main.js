import { By } from "selenium-webdriver";
import { dom } from "../../utils";

export default () => {
  const name = 'Вася';
  it("Entering a {name} and clicking the button should display 'Привет, ${name}!", async () => {
    const expectedResult = "Привет, Вася!";
    await dom.textWhenPossible(By.name('name'), name);
    await dom.clickWhenPossible({ css: ".custom-form__button" });
    const textResult = await driver.findElement(By.className('start-screen__res')).getText();

    if (textResult !== 'Привет, Вася!') {
      throw new Error(
        `Expected ${expectedResult}, but got ${textResult}`
      )
    }
  });
};
